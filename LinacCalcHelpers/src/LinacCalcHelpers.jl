module LinacCalcHelpers

using DataFrames

export typecheck_input, simplify

# check and fix column data ranges
function typecheck_input(data::DataFrame)
    # needs to be > 0
    for n in names(d) ∩ [:f0, :Q0, :rsh]
        d[!, n][d[!, n] .≤ 0] .= 1
    end
    for n in names(d) ∩ [:chi, :P, :phi, :Ke, :Ib]
        d[!, n][d[!, n] .< 0] .= 0
    end
end

# simplify AST
function simplify(s; vars=Dict())
    for (s,v) in vars
        eval(:($(Symbol(s)) = $v))
    end
    if typeof(s) <: Symbol
        try
            a = eval(s)
            if typeof(a) <: Number
                return a
            else
                return s
            end
        catch
            return s
        end
    elseif typeof(s) <: Number
        return s
    else
        try
            return eval(s)
        catch
            for i in 1:size(s.args, 1)
                s.args[i] = simplify(s.args[i])
            end
            return s
        end
    end
end

end # module
