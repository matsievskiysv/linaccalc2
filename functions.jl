function typecheck_input(input::DataFrame)
    # must be in the list
    # must be > 0. Defaults to 1
    for (name in c("f0", "Q0", "rsh", "cell_width")) {
        if (!is.null(cel[[name]]) & (FALSE %in% is.ok(cel[[name]]) |
                                     FALSE %in% (cel[[name]] > 0))) {
            loginfo(paste("Invalid value in", name, "column. Replaced with 1"))
            cel[[name]][which(!is.ok(cel[[name]]))] <- 1
            cel[[name]][which(!cel[[name]] > 0)] <- 1
        }
    }
    # must be >= 0. Defaults to 0
    for (name in c("Ke", "chi", "P", "Ialpha")) {
        if (!is.null(cel[[name]]) & (FALSE %in% is.ok(cel[[name]]) |
                                     FALSE %in% (cel[[name]] >= 0))) {
            loginfo(paste("Invalid value in", name, "column. Replaced with 0"))
            cel[[name]][which(!is.ok(cel[[name]]))] <- 0
            cel[[name]][which(!cel[[name]] >= 0)] <- 0
        }
    }
    # Fill with 0
    for (name in c("Kh", "phi", "Iphi")) {
        if (!is.null(cel[[name]]) & (FALSE %in% is.ok(cel[[name]]))) {
            loginfo(paste("Invalid value in", name, "column. Replaced with 0"))
            cel[[name]][which(!is.ok(cel[[name]]))] <- 0
        }
    }

    cel
}

end
