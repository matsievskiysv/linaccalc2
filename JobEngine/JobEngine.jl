module JobEngine

using Base.Iterators: flatten
using Distributed
using Debugger

import Base: push!, show, run

export JobTree, run!, dump_graph


const GRAPH_BEGINNING = raw"""
\documentclass[tikz]{standalone}
\usetikzlibrary{graphs,graphdrawing,trees}
\usegdlibrary{trees}
\usepackage[european]{circuitikz}
\usepackage{polyglossia}
\setotherlanguage{english}
\setmainfont{LiberationSerif}
\setsansfont{LiberationSans}
\begin{document}
\begin{tikzpicture}[>=stealth, every node/.style={circle, draw, minimum size=1cm}]
\graph [tree layout, grow=down, level distance=1cm, sibling distance=1cm] {
"""
const GRAPH_END = raw"""
};
\end{tikzpicture}
\end{document}
"""

mutable struct JobTreeData{T}
    label::String
    data::Any
    hash::Number
    source::Union{T, Nothing}
    sinks::Vector{T}
end

show(io::IO, z::JobTreeData) = print(io, "JobTreeData $(typeof(z.data)) $(z.label)")

mutable struct JobTreeNode
    label::String
    func::Function
    inputs::Vector{JobTreeData}
    saved_hashes::Vector{Number}
    outputs::Vector{JobTreeData}
    distributed::Bool
    pure::Bool
end

show(io::IO, z::JobTreeNode) = print(io, "JobTreeNode $(z.label)")

mutable struct JobBuffer
    id::UInt
    depends::Vector{UInt}
    funcs::Vector{JobTreeNode}
end

show(io::IO, z::JobBuffer) = print(io, "JobBuffer $(z.id)")

mutable struct JobTree
    funcs::Dict{String, JobTreeNode}
    data::Dict{String, JobTreeData}
    comm_channel::RemoteChannel
    workers::WorkerPool
end
JobTree() = JobTree(Dict(),
                    Dict(),
                    RemoteChannel(()->Channel{UInt}(length(workers()) + 10)),
                    WorkerPool(workers()))

show(io::IO, z::JobTree) = print(io, "JobTree with funcs $(keys(z.funcs)) and data $(keys(z.data))")

struct JobExistsError <: Exception
    var::String
end
Base.showerror(io::IO, e::JobExistsError) = print(io, e.var, " already exists")

struct DataNotFoundError <: Exception
    var::String
end
Base.showerror(io::IO, e::DataNotFoundError) = print(io, e.var, " does not exist")

struct DataRedifinitionError <: Exception
    var::String
end
Base.showerror(io::IO, e::DataRedifinitionError) = print(io, e.var, " already exists")

mutable struct FunctionPacked
    label::String
    func::Function
    inputs::Vector{Any}
    input_labels::Vector{String}
    input_hashes::Vector{Number}
    outputs::Vector{Any}
    output_labels::Vector{String}
    output_hashes::Vector{Number}
    saved_hashes::Vector{Number}
    pure::Bool
end

function push!(obj::JobTree,
               label::String,
               func::Function;
               input::Vector{String}=String[],
               output::Vector{String}=String[],
               distributed::Bool=false,
               pure::Bool=true)
    if get(obj.funcs, label, Nothing) == Nothing
        inputs = []
        for i in input
            if get(obj.data, i, Nothing) == Nothing
                throw(DataNotFoundError(i))
            end
            push!(inputs, obj.data[i])
        end
        outputs = []
        for o in output
            if get(obj.data, o, Nothing) == Nothing
                obj.data[o] = JobTreeData(o, Nothing, 1, Nothing, [])
                push!(outputs, obj.data[o])
            else
                throw(DataRedifinitionError(o))
            end
        end
        jobj = JobTreeNode(label,
                           func,
                           inputs,
                           [1 for j in inputs], # different from data
                           outputs,
                           distributed,
                           length(inputs) > 0 && pure)
        obj.funcs[label] = jobj
        for d in inputs
            push!(d.sinks, jobj)
        end
        for d in outputs
            d.source = jobj
        end
    else
        throw(JobExistsError(label))
    end
end

function find_leafs_rec!(obj::JobTreeNode,
                         leafs::Vector{JobTreeNode},
                         functions::Vector{JobTreeNode})
    push!(functions, obj)
    if length(obj.inputs) == 0
        push!(leafs, obj)
    else
        for i in obj.inputs
            leafs = find_leafs_rec!(i.source, leafs, functions)
        end
        return leafs
    end
end

function find_leafs(obj::JobTree, label::String)::Tuple{Vector{JobTreeNode}, Vector{JobTreeNode}}
    leafs = JobTreeNode[]
    functions = JobTreeNode[]
    find_leafs_rec!(obj.funcs[label], leafs, functions)
    return unique(leafs), unique(functions)
end

function create_job_queue(obj::JobTree,
                          label::String)::Vector{JobBuffer}

    function assign_dependencies!(jobs::JobBuffer,
                                  job_list::Vector{JobBuffer},
                                  all_functions::Vector{JobTreeNode})
        for j in [d.source for d in jobs.funcs[1].inputs if d.source in all_functions],
            b in job_list
            if j in b.funcs
                append!(jobs.depends, [b.id])
            end
        end
    end
    job_list = JobBuffer[]
    leafs, function_pull = find_leafs(obj, label)
    function_all = copy(function_pull)
    job = Nothing
    atomic_list = JobBuffer(length(job_list) + 1, [], [])
    while true
        if job == Nothing
            unique!(leafs)
            if length(leafs) > 0
                job = popfirst!(leafs)
            else
                break
            end
            @debug "processing leaf $job" leafs
        end
        outputs = collect(flatten([d.sinks for d in job.outputs]))
        inputs = [d.source for d in job.inputs]
        if length(inputs ∩ function_pull) ≠ 0
            @debug "not all inputs are ready. postpone $job" leafs
            if length(atomic_list.funcs) > 0
                assign_dependencies!(atomic_list, job_list, function_all)
                append!(job_list, [atomic_list])
                atomic_list = JobBuffer(length(job_list) + 1, [], [])
            end
            append!(leafs, [job])
            job = Nothing
            continue
        elseif length(outputs) > 1
            @debug "$job has multiple outputs $outputs. pushing them to queue"
            append!(atomic_list.funcs, [job])
            filter!((x)->x≠job, function_pull)
            assign_dependencies!(atomic_list, job_list, function_all)
            append!(job_list, [atomic_list])
            atomic_list = JobBuffer(length(job_list) + 1, [], [])
            append!(leafs, outputs ∩ function_pull)
            job = Nothing
            continue
        else
            @debug "buffering $job"
            append!(atomic_list.funcs, [job])
            filter!((x)->x≠job, function_pull)
            if length(outputs) > 0 &&
                length(outputs ∩ function_pull) > 0 &&
                !((outputs ∩ function_pull)[1] in leafs)
                @debug "$(outputs[1]) next in queue"
                job = (outputs ∩ function_pull)[1]
            else
                assign_dependencies!(atomic_list, job_list, function_all)
                append!(job_list, [atomic_list])
                atomic_list = JobBuffer(length(job_list) + 1, [], [])
                job = Nothing
            end
        end
    end
    return job_list
end

function function_pack(obj::JobTree, buffer::JobBuffer)::Vector{FunctionPacked}
    output = []
    for j in buffer.funcs
        push!(output, FunctionPacked(
            j.label,
            j.func,
            [d.data for d in j.inputs],
            [d.label for d in j.inputs],
            [d.hash for d in j.inputs],
            [d.data for d in j.outputs],
            [d.label for d in j.outputs],
            [d.hash for d in j.outputs],
            j.saved_hashes,
            j.pure
        ))
    end
    return output
end

function function_unpack(obj::JobTree, funcs::Vector{FunctionPacked})
    for f in funcs
        j = obj.funcs[f.label]
        j.saved_hashes = f.input_hashes
        for (i, d) in enumerate(j.outputs)
            d.data = f.outputs[i]
            d.hash = f.output_hashes[i]
        end
    end
end

function do_job(funcs::Vector{FunctionPacked}, job_id::UInt, comm::RemoteChannel)::Vector{FunctionPacked}
    for (i, f) in enumerate(funcs)
        if i > 1
            for (j1, n1) in enumerate(funcs[i].input_labels)
                for (j2, n2) in enumerate(funcs[i-1].output_labels)
                    if n1 == n2
                        funcs[i].inputs[j1] = funcs[i-1].outputs[j2]
                        funcs[i].input_hashes[j1] = funcs[i-1].output_hashes[j2]
                        break
                    end
                end
            end
        end
        if (!f.pure) || (f.input_hashes ≠ f.saved_hashes)
            @debug "running function $(f.label)"
            try
                output = f.func(f.inputs...)
                if length(f.output_labels) == 0
                    f.outputs = []
                    f.output_hashes = []
                elseif length(f.output_labels) == 1
                    f.outputs = [output]
                    f.output_hashes = [hash(output)]
                else
                    f.outputs = [x for x in output]
                    f.output_hashes = [hash(o) for o in output]
                end
            catch
                @debug "error running function $(f.label)"
            end
        else
            @debug "skipping function $(f.label)"
        end
    end
    put!(comm, job_id)
    return funcs
end

function run!(obj::JobTree, name::String)::Any
    function wait_fetch()
        @debug "waiting for jobs to finish"
        job_id = take!(obj.comm_channel)
        if job_id == 0
            @debug "interrupted"
            # interrupt()
            return
        end
        i = findfirst(x -> x[1] == job_id, running)
        jj, future = splice!(running, i)
        @debug "fetching job $jj data"
        data = fetch(future)
        function_unpack(obj, data)
        push!(done, jj)
    end
    if name ∉ keys(obj.funcs)
        throw(DataNotFoundError(name))
    end
    jobs = create_job_queue(obj, name)
    done = []
    running = []
    for j in jobs
        while ! (j.depends ⊆ done)
            wait_fetch()
        end
        @debug "spawn $j"
        future = remotecall(do_job, obj.workers, function_pack(obj, j), j.id, obj.comm_channel)
        append!(running, [(j.id, future)])
    end
    # there's no pending jobs. processing the rest
    for f in running
        wait_fetch()
    end
    return [d.data for d in obj.funcs[name].outputs]
end

function dump_graph(obj::JobTree)::String
    output = GRAPH_BEGINNING
    for (_, f) in obj.funcs
        for i in f.inputs
            output *= "$(i.label) [circle] -> $(f.label) [rectangle],\n"
        end
        for o in f.outputs
            output *= "$(f.label) [rectangle] -> $(o.label) [circle],\n"
        end
    end
    output *= GRAPH_END
    return output
end

function dump_graph(obj::JobTree, name::String)::String
    jobs = create_job_queue(obj, name)
    output = GRAPH_BEGINNING
    for j in jobs
        output *= "$(j.id)[draw]//[tree layout]{\n"
        output *= """$(join([f.label for f in j.funcs],"->")),\n"""

        output *= "},\n"
        funcs = join([f.label for f in j.funcs], ",")
        if length(j.depends) > 0
            for d in j.depends
                output *= "$d -> $(j.id),\n"
            end
        else
            output *= "$(j.id),\n"
        end
    end
    output *= GRAPH_END
    return output
end

end # module
