function statl_set1(row::Int, col::Int)::Expr
    if row == col
        out = :(1  - (f^2) / (cdata.f0[row]^2) +
        f * 1im * (1 / cdata.f0[row]) * (1 + cdata.chi[row]) /
        cdata.Q0[col])
    elseif row == col + 1
        out = :(f^2 / (cdata.f0[row] * cdata.f0[col]) *
        cdata.Kh_L[row] / 2 - cdata.Ke_L[row] / 2)
    elseif row == col - 1
        out = :(f^2 / (cdata.f0[row] * cdata.f0[col]) *
        cdata.Kh_R[row] / 2 - cdata.Ke_R[row] / 2)
    else
        out = :(0+0)
    end
    out
end

function statr_set1(col::Int)::Expr
    out = :(1im * 4 * sqrt(pi * f / cdata.f0[row]) *
    sqrt(f * cdata.chi[row] / cdata.Q0[row]) *
    sqrt(cdata.P[row]) * sqrt(constant["fmult"]) *
    exp(1im * pi / 180 * cdata.phi[row]))
    out
end

function preformat_set1(cdata::DataFrame)::DataFrame
    cdata = hcat(cdata, DataFrame(Ke_L=([0; cdata.Ke[1:(end-1)]]),
                                  Ke_R=([cdata.Ke[1:(end-1)]; 0])))
    cdata = hcat(cdata, DataFrame(Kh_L=([0; cdata.Kh[1:(end-1)]]),
                                  Kh_R=([cdata.Kh[1:(end-1)]; 0])))
end

Preformat = Dict("set1" => preformat_set1)
EqStatL = Dict("set1" => statl_set1)
EqStatR = Dict("set1" => statr_set1)

function create(rows::Int,
                set::String
                )::Array{Tuple{Int, Int, Expr}, 2}
    cols = rows + 1
    equations_left = fill((0, 0, :(0+0)), rows, rows)
    equations_right = fill((0, 0, :(0+0)), rows)
    for i = 1:rows, j = 1:rows
        equations_left[i, j] = (i, j, EqStatL[set](i, j))
    end
    for i = 1:rows
        equations_right[i] = (i, cols, EqStatR[set](i))
    end
    hcat(equations_left, equations_right)
end

function preformat_input(cdata::DataFrame, set::String)::DataFrame
    Preformat[set](cdata)
end

function simplify(equations::Array{Tuple{Int, Int, Expr}, 2};
                  cdata::DataFrame,
                  constant::Dict{String, Float64}
                  )::Array{Union{Expr, Complex}, 2}
    function simplification(x)
        s = simplify(x[3], vars=Dict("cdata" => cdata,
                                     "constant" => constant,
                                     "row" => x[1],
                                     "col" => x[2]))
        if typeof(s) <: Number
            s = convert(Complex, s)
        end
        s
    end
    rows = nrow(cdata)
    cols = rows + 1
    pmap(x->simplification(x), equations)
end

function evaluate(equations::Array{Union{Expr, Complex}, 2},
                  f::Vector{T})::Matrix{Complex} where T <: Number
    function solve(equations::Array{Union{Expr, Complex}, 2},
                   f::T)::Array{Complex} where T <: Number
        system = map(x-> simplify(x,
                                  vars=Dict("f" => convert(Complex, f))),
                     equations)
        rows = size(equations)[1]
        system[:, 1:rows] \ system[:, rows + 1]
    end
    rows = size(equations)[1]
    cols = length(f)
    array::Array{Complex, 2} = zeros(rows, cols)
    tmp = pmap(x->solve(equations, x), f)
    for i in 1:length(tmp)
        array[:, i] = tmp[i]
    end
    array
end

