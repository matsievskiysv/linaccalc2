module LinacCalc

include("./JobEngine/JobEngine.jl")
using .JobEngine
export JobTree, run!, dump_graph

end
